﻿using System;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data;

namespace boxDB
{
    public partial class Form1 : Form
    {
        private OleDbConnection connection = new OleDbConnection();

        public Form1()
        {
            InitializeComponent();
            connection.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\Касса\Documents\boxing3\boxers.accdb;
Persist Security Info = False;";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                connection.Open();
                checkConnection.Text = "Connection successfull!";
                OleDbCommand command = new OleDbCommand();
                command.Connection = connection;
                command.CommandText = "insert into sportsman (FirstName, LastName, country, WeightCat) values ('" +
                                            txtFirstName.Text + "', '" +
                                            txtLastName.Text + "', '" +
                                            cmbCountry.Text + "', '" +
                                            cmbWeight.Text + "')";
                command.ExecuteNonQuery();
                MessageBox.Show("Данные сохранены!");
                connection.Close();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }

            dataGridView1.Refresh();
        }

        private void LoadCountry(string columnName)      // Загрузка списка стран в comboBox
        {
            cmbCountry.Items.Clear();
            try
            {
                connection.Open();
                OleDbCommand command = new OleDbCommand();
                command.Connection = connection;
                command.CommandText = "select * from Country";
                OleDbDataReader read = command.ExecuteReader();
                while(read.Read())
                {
                    cmbCountry.Items.Add(read[columnName].ToString());
                }
                connection.Close();

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadCountry("CountryName");
            LoadCombo("category");
            gridLoad();
        }

        DataTable dt_copy = new DataTable();

        private void gridLoad()                 // Загрузка данных в dataGridView
        {
            try
            {
                connection.Open();
                OleDbCommand comm = new OleDbCommand();
                comm.Connection = connection;
                string query = "select * from sportsman";
                comm.CommandText = query;

                OleDbDataAdapter adapter = new OleDbDataAdapter(comm);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                dataGridView1.DataSource = dt;

                dt_copy = dt;

                connection.Close();
            }
            catch (Exception ex)
            {
                checkConnection.Text = "Error" + ex.Message;
            }
        }

        private void LoadCombo(string tableName)            // Загрузка списка весовых категорий в comboBox
        {
            cmbWeight.Items.Clear();
            try
            {
                connection.Open();
                OleDbCommand commande = new OleDbCommand();
                commande.Connection = connection;
                string query = "select * from Weight";
                commande.CommandText = query;
                OleDbDataReader reader = commande.ExecuteReader();
                while (reader.Read())
                {
                    cmbWeight.Items.Add(reader[tableName].ToString());
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void btnSave3_Click(object sender, EventArgs e)
        {
            try
            {
                connection.Open();
                OleDbCommand command2 = new OleDbCommand();
                command2.Connection = connection;
                command2.CommandText = "insert into Weight (category) values ('" + txtWeight.Text + "')";
                command2.ExecuteNonQuery();
                MessageBox.Show("Данные сохранены!");
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }

            LoadCombo("category");
        }

        private void btnUpdateTable_Click(object sender, EventArgs e)
        {
            textBox1.Text = String.Empty;
            //dataGridView1.Refresh();
            checkConnection.Text = dataGridView1.Columns[1].Index.ToString();
            textBox1.Text += "TabIndex = " + dataGridView1.TabIndex + Environment.NewLine;
            DataGridViewRow dgr = new DataGridViewRow();
            textBox1.Text += "IndexOf(DataGridViewRow) = " + dataGridView1.Rows.IndexOf(dgr) + Environment.NewLine;
            textBox1.Text += "ColumnCount = " + dataGridView1.ColumnCount.ToString() + Environment.NewLine;
            textBox1.Text += "Columns[1].Name = " + dataGridView1.Columns[1].Name + Environment.NewLine;
            textBox1.Text += "Columns[1].HeaderText = " + dataGridView1.Columns[1].HeaderText + Environment.NewLine;
            textBox1.Text += "Columns[1].ValueType = " + dataGridView1.Columns[1].ValueType + Environment.NewLine;
            textBox1.Text += "FirstDisplayedCell = " + dataGridView1.FirstDisplayedCell + Environment.NewLine;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            //dataGridView1
        }

        /*
         if (!String.IsNullOrEmpty(txtFilterFirst.Text))
            bindingSource1.Filter = String.Format("FirstName like '%{0}%'", txtFilterFirst.Text);
             for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
             {
                 dataGridView1.Rows[i].Visible = (string)dataGridView1[1, i].Value == txtFilterFirst.Text;
             } 

            textBox1.Text = "";
            textBox1.Text += "Columns.Count = " + dt_copy.Columns.Count + Environment.NewLine;
            textBox1.Text += "Caption[1] = " + dt_copy.Columns[1].Caption + Environment.NewLine;
            textBox1.Text += "Rows.Count = " + dt_copy.Rows.Count.ToString() + Environment.NewLine;
            textBox1.Text += "TableName = " + dt_copy.TableName.ToString() + Environment.NewLine;
            textBox1.Text += "Rows[2] = " + dt_copy.Rows[2].ToString() + Environment.NewLine;
            textBox1.Text += "[1, 3] = " + dataGridView1[1, 3].Value.ToString() + Environment.NewLine;
             */

        private void btnSearch_Click(object sender, EventArgs e)    // Поиск подстроки без учета регистра
        {
            if (!String.IsNullOrEmpty(txtFilterFirst.Text))         // Проверка на пустую строку или нуль
            {
                string[] fn = new string[dataGridView1.Rows.Count];
                for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                {
                    fn[i] = dataGridView1[1, i].Value.ToString().ToLower();
                }

                listBox1.Items.Clear();

                for (int j = 0; j < fn.Length - 1; j++)
                {
                    if (fn[j].Contains(txtFilterFirst.Text.ToLower()))
                    {
                        listBox1.Items.Add(fn[j]);
                    }
                }
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                connection.Open();
                OleDbCommand command = new OleDbCommand();
                command.Connection = connection;
                command.CommandText = "insert into Country (CountryName) values ('" + txtCountry.Text + "')";
                command.ExecuteNonQuery();
                MessageBox.Show("Данные сохранены!");
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            LoadCountry("CountryName");
        }
    }
}
